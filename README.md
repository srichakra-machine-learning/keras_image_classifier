# TensorFlow.js Example: Using an Azure Custom Vision model to detect a Vending Machine Item in a web browser

This example shows you how to use a Machine Learning, which was created with the [Microsoft Azure Custom Vision](https://azure.microsoft.com/en-us/services/cognitive-services/custom-vision-service/) service, in a web browser application.

The [Vending Machine Item images] used in this example, are collected from the client and are not published with this repository.

Prepare the node environments:
```sh
$ npm install
# Or
$ yarn
```

Run the local web server script:
```sh
$ node server.js
```
